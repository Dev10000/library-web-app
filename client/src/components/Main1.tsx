import React, { ReactEventHandler, useState } from 'react';
import Nav from './Nav';
import '../css/Main.css';
import Modal from './Modal';
import Search from './Search2';
import SignUp from './Signup';
import Login from './Login';

function Main1() {
  return (
    <div className="LandingPage">
      <Nav />
      <Modal>
        <Search text="" />
      </Modal>
    </div>
  );
}

export default Main1;
