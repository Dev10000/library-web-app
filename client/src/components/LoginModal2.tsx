import React, { useState } from 'react';
import Modal from 'react-modal';
import { saveAccessTokenToLocalStorage } from '../utils';
import { VisibleModal, RedirectToPage } from '../App';
import '../css/Main.css';

const modalStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		background: 'rgba(144, 238, 144, .8)',
		border: '1px solid gray',
		boxShadow: '5px 5px 5px gray',
	}
};

interface ILoginProps {
	login: (email: string, password: string) => Promise<Response>;
	setToken: (token: string | null) => void;
	setVisibleModal: (modal: VisibleModal) => void;
	visible: boolean;
	setRedirectToPage: (page: RedirectToPage) => void;
}

Modal.setAppElement('#root');

export const LoginModal2: React.FC<ILoginProps> = props => {
	const { login, setToken, setVisibleModal, visible, setRedirectToPage } = props;
	const [infoText, setInfoText] = useState('');

	const onFormSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		const fd = new FormData(event.currentTarget);
		return login(fd.get('email') as string, fd.get('password') as string)
			.then(response => {
				if (response.status === 200) { // login success
					setVisibleModal(null);
					setRedirectToPage('mypage');
					return response.json();
				} else {
					setInfoText('Wrong email or password!');
					return null;
				}
			})
			.then(data => {
				if (data && data.token) {
					saveAccessTokenToLocalStorage(data.token);
					setToken(data.token);
				}
			})
			.catch(err => setInfoText(err.message));
	}

	return (
		<Modal style={modalStyles} isOpen={visible} onRequestClose={() => setVisibleModal(null)}>
			<div>
				<h2>Login</h2>
        <div style={{ color: 'red' }}>{infoText}</div>
        <form className="Login-Form" onSubmit={onFormSubmit}>
					<label htmlFor="email">Email
						<div className="LoginElement">
							<input type="email" name="email" placeholder="Email" defaultValue="test@buutti.com" id="email"/>
						</div>
					</label>
					<label htmlFor="password">Password
						<div className="LoginElement">
							<input type="password" name="password" placeholder="Password" defaultValue="test" id="password"/>
						</div>
					</label>
        <button type="submit" className="LoginBtn">Submit</button>
      </form>
			</div>
		</Modal>
	)
}
