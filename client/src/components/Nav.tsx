import React from 'react';

const Nav: React.FC = () => {
  return (
    <div className="Nav">
      <ul className="NavList">
        <li><a href="#" className="App-Link">Home</a></li>
        <li><a href="#" className="App-Link">Search</a></li>
        <li><a href="#" className="App-Link">Sign Up</a></li>
        <li><a href="#" className="App-Link">Log In</a></li>
      </ul>
    </div>
  );
};

export default Nav;
