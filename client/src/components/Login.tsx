import React from 'react';

interface iLogin {
  email: string;
  password: string;
}

const Login: React.FC<iLogin> = props => {
  const { email, password } = props;
  return (
    <div className="Login">
      <form className="Login-Form">
        <div>
          <h3>Please Login</h3>
        </div>
        <label htmlFor="email">Email</label>
        <div className="LoginElement">
          <input type="email" name={email} placeholder="Email" />
        </div>
        <label htmlFor="password">Password</label>
        <div className="LoginElement">
          <input type="password" name={password} placeholder="Password" />
        </div>
        <button type="submit" className="LoginBtn">Submit</button>
      </form>
    </div>
  );
};

export default Login;