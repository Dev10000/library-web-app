import React from 'react';


interface iSearch {
  text: string;
}

const Search: React.FC<iSearch> = props => {
  return (
    <div className="Search">
      <form className="Form-Search">
        <h3>Search For Books</h3>
        <div className="InputWrapper">
          <input type="text" name={props.text} placeholder="Place your search here..." />
        </div>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Search;