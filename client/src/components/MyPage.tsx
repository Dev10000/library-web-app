import React, { useEffect, useState } from 'react'
import { IUser } from '../../../server/src/types';
import { API_URL } from '../constants';
import { loadAccessTokenFromLocalStorage } from '../utils';

export const MyPage: React.FC<{user: IUser}> = props => { // IUser originally had _id ?
	const { user } = props; // id, name, email
	const [books, setBooks] = useState<any>([]);

	function fetchBorrowedBooks() {
		(async ()=>{
			const res = await fetch(`${API_URL}/user/${user.id}/books`, {
				headers: {
					Authorization: `Bearer ${loadAccessTokenFromLocalStorage()}`,
				},
			});
			const data = await res.json();
			setBooks(data);
		})();
	}

	useEffect(fetchBorrowedBooks, []); // trigger the callback only on component mount

	async function returnCopy(isbn: string,  copyId: string) {
		const res = await fetch(`${API_URL}/books/${isbn}/return/${copyId}`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${loadAccessTokenFromLocalStorage()}`,
			},
		});
		const data = await res.json();
		if (res.status === 200) fetchBorrowedBooks(); // causes re-render as books change
		else alert(data.error);
	}

	return (
	<div>
		<h1>Hi <em style={{color: 'darkgreen'}}>{user.name}</em>, welcome to the library!</h1>
		<button onClick={() => window.location.replace('/editprofile')}>Edit profile</button>
		<h2>You have borrowed these books:</h2>
		{(books.length > 0)
			? books.map((book: any) =>
				<div key={book.isbn} style={{background:'palegreen', padding:'1em', marginBottom: '1em', border: '1px solid darkgreen'}}>
					<div><small><em>{book.author}</em></small></div>
					<div><strong>{book.title}</strong> <small>({new Date(book.published).getFullYear()})</small></div>
					<div><small>{book.subtitle}</small></div>
					<hr/>
					{book.copies.map((copy: any) =>
						<div key={copy.id}>
							Borrowed until <strong>{new Date(copy.due_date).toLocaleDateString()}</strong>
							<button style={{cursor: 'pointer'}} onClick={()=>returnCopy(book.isbn, copy.id)}>Return</button>
						</div>
					)}
				</div>
			)
			: 'No books borrowed.'}
	</div>
	);
};
