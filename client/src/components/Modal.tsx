import React from 'react';
const Modal: React.FC = props => {
  return (
    <div className="Modal">
      {props.children}
    </div>
  );
};

export default Modal;