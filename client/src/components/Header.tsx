import React from 'react'
import { NavLink } from 'react-router-dom';
import './Header.css';

function Header() {
  return (
    <nav>
      <ul>
        <li><NavLink to="/" activeClassName="selected" exact>Home</NavLink></li>
        <li><NavLink to="/" activeClassName="selected">Page 1</NavLink></li>
        <li><NavLink to="/page2" activeClassName="selected">Page 2</NavLink></li>
        <li><NavLink to="/page3" activeClassName="selected">Page 3</NavLink></li>
      </ul>
    </nav>
  )
}

export default Header;