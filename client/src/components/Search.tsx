import React, { useState }  from 'react';
import logo from '../css/logo.svg';
import '../css/Search.css';
import ListItems from './ListItems'
import { AnyCnameRecord } from 'dns';
import { library } from '@fortawesome/fontawesome-svg-core';
import {faTrash} from '@fortawesome/free-solid-svg-icons';
import { isTemplateExpression } from 'typescript';
import axios from 'axios'

library.add(faTrash);

function Search(props: any): any {
  const { user } = props; // id, name, email
  //const [data, setData] = useState('');

  console.log(user)
  
  const [items, setItems]: any = useState([])
  const [currentItem, setCurrentItem]: any = useState({text:"", key:""})

  const addItem = (e: any) => {
    e.preventDefault();
    axios.get(`http://localhost:3001/API/books/?search=${currentItem.text}`)

    .then(response => {
      //response.data.forEach((element: any) => {
        //setCurrentItem({text: JSON.stringify(element, null, 4), key:Date.now()})
        setItems(response.data)
        //addItemF();    
      //});
    })
    
    .catch(error => console.log(error));
  }


  const addItemF = () => {
    const newItem: any = currentItem;
    console.log(newItem)
    if (newItem.text !== "") {
      const items2: any = [...items, newItem];
      setItems(items2)
      setCurrentItem({text: "", key: ""})
    }
  }


  // const addItemF = () => {
  //   const newItem: any = currentItem;
  //   console.log(newItem)
  //   if (newItem.text !== "") {
  //     const items2: any = [...items, newItem];
  //     setItems(items2)
  //     setCurrentItem({text: "", key: ""})
  //   }
  // }

  const handleInput = (e: any) => {
    setCurrentItem({text: e.target.value, key:Date.now()})
  }

  const deleteItem = (key: any) => {
    const filteredItems = items.filter((item: any) => item.isbn !== key);
      setItems(filteredItems)
  }

  const setUpdate = (text: any, key: any) => {
    items.map((item: any) => {if (item.isbn === key) {item.title = text}})
    setItems(items) 
    setCurrentItem({text: "", key: ""}) // Without this line child component "ListItems" doesn update or re-render (editing doesn't work). And I don't know why?
  }

  return (
    <div>
      <h1 id="title-t">SEARCH FOR BOOKS</h1>
    <div className="search-t">
      <header>
      

      <form id="search-form-t" onSubmit={addItem}>
        <input type="text" placeholder="Enter Search String"
        value={currentItem.text}
        onChange={handleInput}/>
        <button type="submit">Search</button>
      </form>

        <ListItems user={user} items={items} deleteItem = {deleteItem} setUpdate={setUpdate}/> 

      </header>
    </div>
    </div>


  );
  
}

export default Search;
