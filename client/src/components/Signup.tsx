import React, { useState} from 'react';
import { useForm }from "react-hook-form"
import Modal from 'react-modal'
import '../css/Signup.css';
import axios from 'axios'

// npm install react-hook-form
// How to use react-hook-form - https://www.youtube.com/watch?v=bU_eq8qyjic

interface iSignUp {
  name: string;
  email: string;
  password: string;
}

// const SignUp: React.FC<iSignUp> = props => {
//   const { name, email, password } = props;
//   return (

export default SignupModal;

function SignupModal({open, onClose}: any): any {
  //const [modalIsOpen, setModalIsOpen] = useState(false)

  // COMPONENT STATE
  const {register, handleSubmit, errors} = useForm();
  const [responseErr, setResponseErr] = useState();
  const [responseErr2, setResponseErr2] = useState();
  const [responseErrTest, setResponseErrTest] = useState();
  const [responseData, setResponseData]: any = useState();
  // COMPONENT STATE
  
  const onSubmit = (data: any) => {
    //data.preventDefault();
    axios.post('http://localhost:3001/API/user', data)
  
    .then(response => {
        setResponseData(JSON.stringify(response.data))
      })
    
    .catch(err => {

      // TESTING *********
    //.catch((err: any) => setResponseErr(err.response.data.toString()))
    //.catch((err: any) => setResponseErr(err.response))
      let newErr1: any = Object.entries(err)
      newErr1 = JSON.stringify(newErr1, null, 4)
      let newErr2: any = Object.values(err)
      let newErr3: any = Object.keys(err)
      let newErr5: any = err.response
      // Use <pre> tag or textarea to get JSON pretty print to work
      newErr5 = JSON.stringify(newErr5, null, 2)
      console.log(err)
      setResponseErrTest(newErr1)
      // TESTING *********
  
      setResponseErr(err.response.data)
      setResponseErr2(err.toString())
    })
  }

  //const { name, email, password } = props;
  return (
    <div>
        {/* <button onClick={() => setModalIsOpen(true)}>Button 1</button> */}
      <Modal
        isOpen={open}
        onRequestClose={onClose}
        className="modal"
        overlayClassName="modal-overlay">

        <div className="signup">
          <form className="Form" onSubmit={handleSubmit(onSubmit)}>
            <div className="form-container">
            <div className="FormElement">
              <label htmlFor="name">Name</label>
              <input type="text"  placeholder="Name" name="name" ref={register({ required: "Name Required"})} />
              {errors.name && <p>{errors.name.message}</p>}
            </div>

            <div className="FormElement">
              <label htmlFor="email">Email</label>
              <input type="email"  placeholder="Email" name="email" ref={register({ required: "Email Required"})} />
              {errors.email && <p>{errors.email.message}</p>}
            </div>

            <div className="FormElement">
              <label htmlFor="password">Password</label>
              <input type="password"  placeholder="Password" name="password" ref={register({ required: "Password Required", minLength: {value: 5, message: "Password Min 5 Characters"}})} />
              {errors.password && <p>{errors.password.message}</p>}
            </div>

            <div className="signup">
            <button type="submit">Submit</button>
            <button onClick={onClose}>Close Modal</button>
            </div>
            </div>

            <div className="signup-message">
              <p>{responseData}</p>
              <p>{responseErr}</p>
              <p>{responseErr2}</p>
            </div>

            <div>
              <p>Test Area</p>
              <textarea className="pretty-print"  value={responseErrTest} />
            </div>

          </form>
        </div>

      </Modal>
    </div>
  )
}



