import React from 'react';
import '../css/ListItems.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FlipMove from 'react-flip-move';
import axios from 'axios'
import { LoginFn } from '../AuthContext';
import {loadAccessTokenFromLocalStorage} from '../utils';
import { copyFile } from 'fs';

function ListItems(props: any) {
  const items = props.items;
  let userId = props.user
  console.log(userId)
  if (userId === null) {
    userId = {id: 0, name: "", email: ""}
  }

  const listBorrowedBooks = (isbn: any) => {
    console.log("user " + userId.id)
    console.log(items)
    const filterISBN: any = items.filter((copy: any) => copy.isbn === isbn)
    //console.log("borrow isbn " + JSON.stringify(filterISBN, null, 4))
    //console.log("borrow isbn " + JSON.stringify(filterISBN[0].copies, null, 4))
    const borrowedBooks = filterISBN[0].copies.filter((item: any) => item.borrower_id === userId.id)
    console.log("borrow " +  JSON.stringify(borrowedBooks, null, 4))
    return borrowedBooks.length
  }

  const booksInLibrary = (key: any) => {
    //const filteredItems = items.filter((item: any) => item.isbn !== key);
    const filterISBN: any = items.filter((x: any) => x.isbn === key)
    //console.log(filterISBN)
    const booksInLibrary: any = filterISBN[0].copies.filter((x: any) => x.status === "in_library").length
      //setItems(filteredItems)
    return booksInLibrary
  }

  const getAvailableBookId = (isbn: any) => {
    const filterISBN: any = items.filter((x: any) => x.isbn === isbn)
    const availableBook: any = filterISBN[0].copies.filter((x: any) => x.status === "in_library")
      //console.log(availableBook[0].id)
      //setItems(filteredItems)
      let book;
      if (availableBook[0]) { 
      book = availableBook[0].id
      } else book = 1
    return book
  }

  const getBorrowedBookId = (isbn: any) => {
    const filterISBN: any = items.filter((x: any) => x.isbn === isbn)
    const availableBook: any = filterISBN[0].copies.filter((x: any) => x.status === "in_library")
      //console.log(availableBook[0].id)
      //setItems(filteredItems)
      let book;
      if (availableBook[0]) { 
      book = availableBook[0].id
      } else book = 1
    return book
  }

  const returnBook = (isbn: any) => {

    // Filter matching ISBN and userID from the books object
    const filterISBN: any = items.filter((copy: any) => copy.isbn === isbn)
    let borrowedBooks = filterISBN[0].copies.filter((item: any) => item.borrower_id === userId.id)

    // Check that the filtered array is not undefined
    let bookId
    if (borrowedBooks[0]) {
      bookId = borrowedBooks[0].id
    } else bookId = 0

    // Request
    axios.defaults.headers.common = {'Authorization': `bearer ${loadAccessTokenFromLocalStorage()}`}
    axios.patch(`http://localhost:3001/API/books/${isbn}/return/${bookId}`)

    .then(response => {
      window.alert("Returned " + JSON.stringify(response.data, null, 4))
      props.deleteItem(isbn)
    })
    
    .catch(err => {
      window.alert(JSON.stringify(err.response.data.error, null, 4))
      //window.alert(err.response.data.error.toString())
    })
  }

  const borrowBook = (isbn: any, id: any) => {
    //e.preventDefault();
    axios.defaults.headers.common = {'Authorization': `bearer ${loadAccessTokenFromLocalStorage()}`}
    axios.patch(`http://localhost:3001/API/books/${isbn}/borrow/${id}`)

    .then(response => {
      //response.data.forEach((element: any) => {
        //setCurrentItem({text: JSON.stringify(element, null, 4), key:Date.now()})
        window.alert("Borrowed " + JSON.stringify(response.data.title, null, 4))
        //console.log(response.data)
        //props.setItems()
        props.deleteItem(isbn)
        //addItemF();    
      //});
    })
    
    .catch(err => {
      window.alert(JSON.stringify(err.response.data.error, null, 4))
      //window.alert(err.response.data.error.toString())
      let newErr1: any = Object.entries(err)
      newErr1 = JSON.stringify(newErr1, null, 4)
      console.log(newErr1)
      
    })
  }

  const listItems = items.map((item: any) =>
    <div className="list-t" key={item.isbn}>

      <div className="search-icons">
        <FontAwesomeIcon className="faicons-t" icon="trash"
        onClick = { () => props.deleteItem(item.isbn)}/>
      </div>
    
      <div className="title" id={item.isbn}>
        {item.title + (item.subtitle ? ": " + item.subtitle : " ")}
      </div>

      <div className="author" id={item.isbn}>
        {"by " + item.author + " | " + new Date(item.published).toLocaleDateString('en-EN', {day:'numeric', month:'short', year:'numeric'})}
      </div>

      <div className="book-func">

      <div className="book-copies" id={item.isbn}> 
         {"Books in library: " + booksInLibrary(item.isbn)}
      </div>

      {props.user &&
      <div className="borrowed-book" id={item.isbn}>
        {" | Borrowed Book: " + listBorrowedBooks(item.isbn) + " pc "}
      </div>
      }

      {props.user &&
        <div className="book-borrow" onClick = { () => borrowBook(item.isbn, getAvailableBookId(item.isbn))} id={item.isbn}>
          {" |  Borrow the Book "}
        </div>
      }

      {props.user &&
        <div className="book-return" onClick = { () => returnBook(item.isbn)}  id={item.isbn}>
          {" |  Return the Book"}
        </div>
      }
      </div>
        
    </div>
  )

  return(
  <div>
    <FlipMove duration={300} easing="ease-in-out">
    {listItems}
    </FlipMove>
  </div>
  )

}
export default ListItems;