import React, { useState, useEffect } from 'react';
import { IUser } from '../../../server/src/types';
import { loadAccessTokenFromLocalStorage } from '../utils';
import axios from 'axios';
import '../css/edit-profile.css';


const EditProfile: React.FC<{user: IUser}> = props => {
  const { user } = props;
   const [infoText, setInfoText] = useState('');
   const [error, setError] = useState<string | null | undefined>('');
   const [name, setName] = useState<string>('');
   const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  

  function handleNameChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
     setName(e.currentTarget.value);
   }
  function handleEmailChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
     setEmail(e.currentTarget.value);
   }
  function handlePasswordChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
     setPassword(e.currentTarget.value);
  }

  function takeMeToMyPage() {
    window.location.replace('/mypage');
  }

  const updateUserHandler = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    try {
      const min = 2, max = 30, pattern = /^[a-zA-ZäåÄÅ\s*]+$/g;
      const passLen = 5;
      const data = {
      name,
      email,
      password
      };
      if (data.name === '' && data.email === '' && data.password === '')
        return setError('Name, email or password required');
      
      if (data.name !== '') {
        if (data.name.length < min) {
          return setError('Name is too short');
        } else if (data.name.length > max) {
          return setError('Name is too long');
        } else if (data.name.length < min) {
          return setError('Name is too short');
        } else if (!pattern.test(data.name)) {
          return setError('Name contains invalid characters');
        }
        data.name = data.name.replace(/\s{1,}/g, ' ').trim();
      }
      else delete data.name;

      if (data.password !== '') {
        if (data.password.length < passLen) {
          return setError('Password is too short');
        } else if (data.password.length > max) {
          return setError('Password is too long');
        }
        data.password = data.password.trim();
      }
      else delete data.password;

      if (data.email !== '') {
        if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/g.test(data.email)) {
          return setError('Please provide a valid email');
        }
      }
      else delete data.email;

      JSON.stringify(data);
      const res = await axios({
        method: 'PATCH',
        url: `http://localhost:3001/API/user/${user.id}`,
        data,
        headers: {
          Authorization: `Bearer ${loadAccessTokenFromLocalStorage()}`,
          'Content-type': 'Application/json'
        }
      });
      if (res.status === 200) {
        setName('');
        setEmail('');
        setPassword('');
        setError('');
        setInfoText('User updated successfully');
        window.setTimeout(() => {
          window.location.replace('/mypage');
        }, 1000);
      }
    } catch (err) {
      console.log(err);
      if (err.response.data === 'User already registered!') {
        setError(err.response.data);
      }
      setError(err.response.data.error);
      setTimeout(() => {
        setError(err.response.data = null);
      }, 1000);
    }
  }

  return (
        <div className="EditProfile">
          <h2 className="EditHeading">edit your profile</h2>
          <p className="ErrorMessage">{error}</p>
          <p className="SuccessMessage">{infoText}</p>
          <form onSubmit={updateUserHandler} className="EditForm">
            <label htmlFor="text">Name
              <div className="EditElement">
                <input type="text" name="name" placeholder={user.name} id="name" value={name} onChange={handleNameChange} />
              </div>
            </label>
            <label htmlFor="email">Email
              <div className="EditElement">
                <input type="email" name="email" placeholder={user.email} id="email" value={email} onChange={handleEmailChange} />
              </div>
            </label>
            <label htmlFor="password">Password
              <div className="EditElement">
                <input type="password" name="password" placeholder="**********" id="password" value={password} onChange={handlePasswordChange} />
              </div>
        </label>
        <div className="ButtonWrapper">
            <button type="submit" className="EditBtn Primary">Update</button>
            <button type="button" className="EditBtn CancelBtn" onClick={takeMeToMyPage}>Cancel</button>
        </div>
          </form>
        </div>
  );
 };

export default EditProfile;
