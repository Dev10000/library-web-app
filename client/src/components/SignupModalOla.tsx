import React, { useState } from 'react';
import Modal from 'react-modal';
import axios from 'axios';
import '../css/signup-ola.css';

interface displayOnCondition {
  open: boolean;
  onClose: () => void;
}

const SignupModalOla: React.FC<displayOnCondition> = props => {
  const { open, onClose } = props;
   const [infoText, setInfoText] = useState('');
   const [error, setError] = useState<string | null | undefined>(null);
   const [name, setName] = useState<string>('');
   const [email, setEmail] = useState<string>('');
   const [password, setPassword] = useState<string>('');

  function handleNameChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
     setName(e.currentTarget.value);
   }
  function handleEmailChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
     setEmail(e.currentTarget.value);
   }
  function handlePasswordChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
     setPassword(e.currentTarget.value);
   }

  const signupHandler = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    try {
      const min = 2, max = 30, pattern = /^[a-zA-ZäåÄÅ\s*]+$/g;
      const passLen = 5;
      const data = {
      name,
      email,
      password
      };
      if (data.name === '' && data.email === '' && data.password === '')
        return setError('Name, email and password required');
      if (data.name === '') {
        return setError('Name is required');
      } else if (data.name.length < min) {
        return setError('Name is too short');
      } else if (data.name.length > max) {
        return setError('Name is too long');
      } else if (data.name.length < min) {
        return setError('Name is too short');
      } else if (!pattern.test(data.name)) {
        return setError('Name contains invalid characters');
      }
      data.name = data.name.replace(/\s{1,}/g, ' ').trim();
      if (data.password === '') {
        return setError('Password is required');
      } else if (data.password.length < passLen) {
        return setError('Password is too short');
      } else if (data.password.length > max) {
        return setError('Password is too long');
      }
      data.password = data.password.trim();
      if (data.email === '') {
        return setError('Email is required');
      } else if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/g.test(data.email)) {
        return setError('Please provide a valid email');
      }
      const res = await axios.post('http://localhost:3001/API/user', data)
      if (res.status === 200) {
        setName('');
        setEmail('');
        setPassword('');
        setError('');
        setInfoText('User created successfully');
        window.setTimeout(() => {
          window.location.replace('/signupsuccessful');
        }, 2000);
      }
    } catch (err) {
      if (err.response.data === 'User already registered!') {
        setError(err.response.data);
      }
      setError(err.response.data.error);
      setTimeout(() => {
        setError(err.response.data = null);
      }, 2000);
    }
  }

  return (
    <div>
      <Modal isOpen={open}
        onRequestClose={onClose}
        className="Modal"
        overlayClassName="Overlay">
        <div>
          <h2 className="SignupHeading">sign up</h2>
          <p className="ErrorMessage">{error}</p>
          <p className="SuccessMessage">{infoText}</p>
          <form onSubmit={signupHandler}>
            <label htmlFor="text">Name
              <div className="SignupElement">
                <input type="text" name="name" placeholder="Name" id="name" value={name} onChange={handleNameChange} />
              </div>
            </label>
            <label htmlFor="email">Email
              <div className="SignupElement">
                <input type="email" name="email" placeholder="Email" id="email" value={email} onChange={handleEmailChange} />
              </div>
            </label>
            <label htmlFor="password">Password
              <div className="SignupElement">
                <input type="password" name="password" placeholder="Password" id="password" value={password} onChange={handlePasswordChange} />
              </div>
            </label>
            <button type="submit" className="SignupBtn">Submit</button>
          </form>
        </div>
      </Modal>
    </div>
  );
 };

export default SignupModalOla;
