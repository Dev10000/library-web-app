import React, { ReactEventHandler, useState } from 'react';
import Nav from './Nav';
import '../css/Main.css';
import Modal from './Modal';
import Search from './Search2';
import SignUp from './Signup';
import Login from './Login';

function Main2() {
  return (
    <div className="LandingPage">
      <Nav />
      <Modal>
        <Search text="" />
        <SignUp name="Ola" email="ola@email.com" password="secret" />
        <Login email="ola@email" password="secret" />
      </Modal>
      <div className="Main">
        <h1>Welcome to Green Library!</h1>
      </div>
    </div>
  );
}

export default Main2;
