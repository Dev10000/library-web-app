import React, { useState } from 'react';
import Modal from 'react-modal';
import { saveAccessTokenToLocalStorage } from '../utils';
import { VisibleModal, RedirectToPage } from '../App';

const modalStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		background: 'rgba(144, 238, 144, .8)',
		border: '1px solid gray',
		boxShadow: '5px 5px 5px gray',
	}
};

const inputStyles: React.CSSProperties = {
		display: 'block',
		marginBottom: '1em',
};

const buttonStyles: React.CSSProperties = {
	display: 'block',
	marginBottom: '1em',
	cursor: 'pointer',
};

interface ILoginProps {
	login: (email: string, password: string) => Promise<Response>;
	setToken: (token: string | null) => void;
	setVisibleModal: (modal: VisibleModal) => void;
	visible: boolean;
	setRedirectToPage: (page: RedirectToPage) => void;
}

Modal.setAppElement('#root');

export const LoginModal: React.FC<ILoginProps> = props => {
	const { login, setToken, setVisibleModal, visible, setRedirectToPage } = props;
	const [infoText, setInfoText] = useState('');

	const onFormSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		const fd = new FormData(event.currentTarget);
		return login(fd.get('email') as string, fd.get('password') as string)
			.then(response => {
				if (response.status === 200) { // login success
					setVisibleModal(null);
					setRedirectToPage('mypage');
					return response.json();
				} else {
					setInfoText('Wrong email or password!');
					return null;
				}
			})
			.then(data => {
				if (data && data.token) {
					saveAccessTokenToLocalStorage(data.token);
					setToken(data.token);
				}
			})
			.catch(err => setInfoText(err.message));
	}

	return (
		<Modal style={modalStyles} isOpen={visible} onRequestClose={() => setVisibleModal(null)}>
			<div>
				<h2>Login</h2>
				<div style={{color:'red'}}>{infoText}</div>
				<form onSubmit={onFormSubmit}>
					<label>Email<input style={inputStyles} type="email" name="email" defaultValue="test@buutti.com"/></label>
					<label>Password<input style={inputStyles} type="password" name="password" defaultValue="test"/></label>
					<button style={buttonStyles}>Login</button>
				</form>
			</div>
		</Modal>
	)
}
