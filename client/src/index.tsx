import React from 'react';
import ReactDOM from 'react-dom';
import AuthHandler from './AuthHandler';
import './css/index.css';

ReactDOM.render((
<React.StrictMode>
  <AuthHandler/>
</React.StrictMode>
), document.getElementById('root'));
