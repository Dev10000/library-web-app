import React, { useState } from 'react';
//import './css/App.css';
import { BrowserRouter as Router, Route, Switch, Redirect, NavLink } from 'react-router-dom';
import { LoginFn } from './AuthContext';
import { IUser } from '../../server/src/types';

import Search from './components/Search';
import { LoginModal2 } from './components/LoginModal2';
import SignupModalOla from './components/SignupModalOla';
import EditProfile from './components/EditProfile';
import { MyPage } from './components/MyPage';

const activeStyle = { color: 'darkgreen' };

interface IApp {
  userIsAuthenticated: boolean
  onLogoutClick: () => void,
  login: LoginFn,
  user: IUser | null,
  setToken: (value: string | null) => void
}

export type VisibleModal = "login" | "signup" | null;
export type RedirectToPage = "mypage" | "signupsuccessful" | null;

const App: React.FC<IApp> = (props) => {
  const [modalIsOpen, setModalIsOpen] = useState(false)
  const {
    userIsAuthenticated,
    login,
    user,
    onLogoutClick,
    setToken
  } = props;

  const [visibleModal, setVisibleModal] = useState<VisibleModal>(null);
  const [redirectToPage, setRedirectToPage] = useState<RedirectToPage>(null);

  const setModal = (modalValue: VisibleModal) =>
    (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
      event.preventDefault();
      visibleModal === modalValue ?
        setVisibleModal(null) :
        setVisibleModal(modalValue);
    };

  return (
    <div>
      <Router>
        {redirectToPage && <Redirect to={redirectToPage} />}
        <nav>
          <NavLink exact to="/" activeStyle={activeStyle}>Library</NavLink>
          <NavLink to="/search" activeStyle={activeStyle}>Search</NavLink>
          {
            userIsAuthenticated ?
              <NavLink activeStyle={activeStyle} to="/mypage">My page</NavLink> :
              <div className="menu" onClick={() => setModalIsOpen(true)}>Signup</div>
          }
          {
            userIsAuthenticated ?
              <a href="#logout" onClick={onLogoutClick}>Logout</a> :
              <a href="#login" onClick={setModal('login')}>Login</a>
          }
        </nav>
        <Switch>
        <Route exact path="/">
            <h1>Welcome to <span style={activeStyle}>Green</span> e-library!</h1>
            <h2 style={activeStyle}>You can borrow books from the Search page while logged in.</h2>
          </Route>
          <Route path="/search"><Search user={user} /></Route>
          <Route path="/mypage">{user ? <MyPage user={user} /> : <div>You are logged out.</div>}</Route>
          <Route path="/editprofile">{user ? <EditProfile user={user} /> : <div>You are logged out.</div>}</Route>
          <Route path="/signupsuccessful"><h1>Signup successful!</h1>You can now login.</Route>
        </Switch>
      </Router>

      {/* <SignupModal open={modalIsOpen} onClose={() => setModalIsOpen(false)} /> */}
      <SignupModalOla open={modalIsOpen} onClose={() => setModalIsOpen(false)} />
      <LoginModal2
            login={login}
            setToken={setToken}
            visible={visibleModal === "login" ? true : false}
            setVisibleModal={setVisibleModal}
            setRedirectToPage={setRedirectToPage}
          />
    </div>
  );
};

export default App;
