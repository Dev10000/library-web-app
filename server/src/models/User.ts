import { createSalt, createHash } from "./utils";
import { compare } from "bcrypt";
import { v1 as uuidv1 } from 'uuid';

export interface IUser {
    id: string;
    name: string;
    password: string;
    email: string;
    comparePassword(passwordToCompare?: string): boolean;
}

class User {
    id: string;
    name: string;
    email: string;
    //private only in before compile; NOT AT RUNTIME! If you send this object to 
    //JSON.serialize as is, these will be present in the resulting string, which 
    //might then end up in the frontend.
    private hash: string; 
    private salt: string; //https://auth0.com/blog/adding-salt-to-hashing-a-better-way-to-store-passwords/

    static findAll: () => Promise<User[]>;
    static findOne: (email: string | undefined, id: string | undefined) => Promise<User | undefined>;
    static findById: (id: string) => Promise<User | undefined>;
    static save: (user: User) => Promise<User>;


    constructor(name: string, email: string, password: string) {
        this.id = uuidv1();
        this.name = name;
        this.email = email;

        this.salt = createSalt();
        this.hash = createHash(password, this.salt);
    };

    toJson = () => JSON.stringify({
        id: this.id,
        name: this.name,
        email: this.email
    });

    comparePassword = (otherPw: string): Promise<boolean> => compare(otherPw, this.hash);

    changePassword = (newPw: string) => {
        this.salt = createSalt();
        this.hash = createHash(newPw, this.salt);
    };
}


// Ola: I had to write the users.json variable here 
//const users: User[] = JSON.parse(fs.readFileSync(userDbFile, 'utf-8'));
const users: User[] = [];
const testUser =  new User('Teppo Testaaja', 'test@buutti.com', 'test');
// Testuser should have the same id every time as we dont want to change it in postman on every run
testUser.id = "a892a360-d720-11ea-87d0-0242ac130003";
users.push(testUser);


// returns the whole users array
const findAll = async (): Promise<User[]> => {
    return users; // without async: return Promise.resolve(users) is required
};

// returns a single user by email or id
const findOne = async (email: string | undefined, id: string | undefined): Promise<User | undefined> => {
    const user = users.find(user => user.email === email  ||  user.id === id);
    return user;
};

// returns a single user by id
const findById = async (id: string): Promise<User | undefined> => {
    const user = users.find(user => user.id === id);    
    return Promise.resolve(user);
};

// adds a user to users array and saves the user database
const save = async (user: User): Promise<User> => {
    users.push(user);
    return Promise.resolve(user);
};

User.findAll = findAll;
User.findOne = findOne;
User.findById = findById;
User.save = save;

export default User;
