import { Router } from "express";
import loginRoutes from "./loginRoutes";
import { logoutHandler } from "../controllers/loginController";
import { refreshTokenHandler } from "../controllers/refreshTokenController";
import userRoutes from "./userRoutes";
import libraryRoutes from "./libraryRoutes";

export const createRoutes = (): Router => {
    const router = Router();
    router.use("/login", loginRoutes());
    router.use("/logout", logoutHandler);
    router.get("/refreshToken", refreshTokenHandler);
    router.use("/user", userRoutes());
    router.use("/books", libraryRoutes());
    return router;
};
