import { Router } from "express";
import { searchBookHandler, getBookByIsbn, borrowBookHandler, returnBookHandler } from "../controllers/libraryController";
import passport from "passport";

export default (): Router => {
    const router = Router();
//    router.post("/", postBookHandler);
    router.get(
        "/", // /?search=SearchQuery
        searchBookHandler
    );
    router.get(
        "/:isbn",
        getBookByIsbn
    );
    router.patch(
        "/:isbn/borrow/:id",
        passport.authenticate("jwt", { session: false }),
        borrowBookHandler
    );
    router.patch(
        "/:isbn/return/:id",
        passport.authenticate("jwt", { session: false }),
        returnBookHandler
    );
    return router;
};
