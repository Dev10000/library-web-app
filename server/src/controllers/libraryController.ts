import { Request, Response } from "express";
import { IBook, ICopy } from "../types";


// sends an array of book objects searched by title or author
export const searchBookHandler = async (
    req: Request,
    res: Response
): Promise<Response> => {
		const { search } = req.query;
		if ( !(typeof search === 'string' && search.trim() !== '') ) return res.status(500).json({error: 'No search query!'});

		const result = global.books.filter((book: IBook)=> {
			const haystack = book.title + book.author;
			const re = new RegExp(search, 'i');
			const idx = haystack.search(re);
			if (idx !== -1) return true; // match found, push book to found results
		});
		return res.status(200).json(result);
};


// sends the book object with given ISBN, or error if not found
export const getBookByIsbn = async (
    req: Request,
    res: Response
): Promise<Response> => {
		const { isbn } = req.params;
		const book = global.books.find(book => book.isbn === isbn);
		if (book){return res.status(200).json(book); }   
		return res.status(404).json({ error: 'No book found with given ISBN.'});
};


// sends the borrowed book object that includes only the copy that was borrowed, or error if the book is not available
export const borrowBookHandler = async (
	req: Request,
	res: Response
): Promise<Response> => {
	const { isbn, id } = req.params; // book_isbn, copy_id
	if (req.user === undefined) return res.status(500).json({ error: 'Unauthorized.' }); // req.user contains the logged in user object

	const book = global.books.find(book => book.isbn === isbn);
	if (book === undefined) return res.status(500).json({ error: 'No book found with given ISBN.' });

	const availableCopy = book.copies.find(copy => copy.status === 'in_library'  &&  copy.id === id);
	if (availableCopy === undefined) return res.status(500).json({ error: 'The copy is not available.' });

	else {
		availableCopy.borrower_id = req.user.id;
		availableCopy.status = 'borrowed';
		const due = new Date();
		due.setDate(due.getDate() + 28); // four weeks from now :D
		availableCopy.due_date = due.toISOString();
		//books.save();
		const borrowedBook = { ...book, copies:[availableCopy]};
		return res.status(200).json(borrowedBook);
	}
};

export const returnBookHandler = async (
	req: Request,
	res: Response
): Promise<Response> => {
	const { isbn, id } = req.params;

	const book: any = global.books.find(book => book.isbn === isbn);
	if (!book) return res.status(500).json({ error: 'No book found with given ISBN.' });

	const availableCopy = book.copies.find((copy: any) => copy.status === 'borrowed' && copy.id === id);
	if (!availableCopy) return res.status(500).json({ error: 'The copy is not borrowed. Please also check that you are using correct borrower id' });

	else {
		availableCopy.borrower_id = null;
		availableCopy.status = 'in_library';
		availableCopy.due_date = null;

		return res.status(200).json(availableCopy);}
};
