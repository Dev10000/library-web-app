import { Request, Response } from "express";
import User, { IUser } from "../models/User";
import { IBook } from "../types";


const saveUserIfNotExists = async (user: User) =>
    User.findOne(user.email, undefined)
        .then(userFound => {
            if (userFound) throw new Error("User already registered!");
            return User.save(user);
        }); // .catch((err) => console.log("CATCH"));


export const postUserHandler = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const { name, email, password } = req.body as IUser;
    if (typeof name !== 'string'  ||  typeof email !== 'string'  ||  typeof password !== 'string') return res.status(500).json({error:'Name, email and password required.'});
    if (name.trim() === '') return res.status(500).json({error:'Invalid name.'});
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) return res.status(500).json({error:'Invalid email.'});
    if (password.trim() === '') return res.status(500).json({error:'Invalid password.'});

    const user = new User(name, email, password);

    return saveUserIfNotExists(user)
        .then(() => res.status(200).send(user.toJson()))
        .catch((err: Error) => res.status(500).json({error:err.message}));
};


export const patchUserHandler = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const users = await User.findAll();
    // allow only changing currently logged user_id data
    if (req.user !== undefined  &&  req.user.id !== req.params.userId) return res.status(500).json({error:'Invalid user.'});

    const { name, email, password } = req.body as IUser;
    if (typeof name !== 'string'  &&  typeof email !== 'string'  &&  typeof password !== 'string') return res.status(500).json({error:'Name, email or password required.'});
    if (name !== undefined  &&  name.trim() === '') return res.status(500).json({error:'Invalid name.'});
    if (email !== undefined  &&  !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) return res.status(500).json({error:'Invalid email.'});
    if (password !== undefined  &&  password.trim() === '') return res.status(500).json({error:'Invalid password.'});

    return User.findById(req.params.userId)
        .then(user => {
            if (user === undefined) return res.status(404).send("User not found.");
            if (email !== undefined) {
                const sameEmail = users.filter(user => user.id !== req.params.userId && user.email === email);
                if (sameEmail.length > 0) return res.status(500).json({error:'Email is already in use.'});
                user.email = email;
            }
            if (name !== undefined) user.name = name;
            if (password !== undefined) user.changePassword(password);
            return res.status(200).send(user.toJson());
        })
        .catch((err: Error) => res.status(500).json(err.message));
};


export const getUserHandler = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const { userId } = req.params;

    /*
        If your project is not small, you should consider using something like
        class-transformer to guard against showing user information that they should
        not be able to see. https://github.com/typestack/class-transformer
    */

    return User.findById(userId)
        .then(user => user ?
            res.status(200).send(user.toJson()) :
            res.status(404).send("User not found.")
        )
        .catch((err: Error) => res.status(500).send(err.message));
};


const getMyCopies = (book: IBook, userId: string) => {
    return book.copies.filter((copy) => userId && copy.borrower_id === userId )
};


export const listBorrowedBooks = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const { userId } = req.params;
    const found_books = global.books
    .filter(book => {
        const mycopies = getMyCopies(book, userId);
        return (mycopies.length > 0)
    }).map(book => {
        const mycopies = getMyCopies(book, userId);
        return {...book, copies: mycopies}
    })

    return found_books ? 
        res.status(200).send(found_books) : 
        res.status(404).send({ error: "notfound" });
};
