import express, { NextFunction, Request, Response } from "express";
// import { setupDatabase } from "./database";
import { PORT } from "./constants";
import { createRoutes } from "./routes/routes";
import { setupAuthenticationStrategies } from "./authentication";
import cors from "cors";
import cookieParser from "cookie-parser";
import { IUser } from "./models/User";
import books_json from "../db/books-dummy.json";
import { IBook } from "./types";

// Alter express User type to match ours. Enables us to use the right type in controllers.
// https://github.com/DefinitelyTyped/DefinitelyTyped/issues/23976
declare global {
    namespace Express {
        interface User extends IUser {}
    }
    namespace NodeJS {
        interface Global {
            books: IBook[];
            requestCounter: number;
        }
    }
}
global.books = books_json as IBook[];
global.requestCounter = 0;

const app = express();

if (process.env.NODE_ENV !== "production")
    app.use(cors({
        origin: "http://localhost:3000",
        credentials: true,
        methods: "GET, PUT, POST, PATCH, DELETE"
    })); // Needed for development use for production set allowed origins and methods.
app.use(cookieParser());
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());


// logging middleware
app.use((req, res, next) => {
    console.log(new Date(), "Request", ++global.requestCounter, req.method, req.url, (req.method !== "GET") ? req.body : "");
    next();
});


// catch SyntaxErrors like when receiving ill-formed json
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    if (err instanceof SyntaxError) {
        const error = { status: undefined, type: undefined, ...err };
        if (error.status === 400 && "body" in err) {
            console.error(error.type);
            res.status(400).json({ error: "Bad JSON.", message: error.message, type: error.type });
        }
        else { next(); }
    }
});

app.use("/api", createRoutes());
app.get("/", (_req, res) => res.send("Hello from the library server!"));
setupAuthenticationStrategies();

app.listen(PORT, () => {
    console.log(`Server listening on http://localhost:${PORT}`);
});
