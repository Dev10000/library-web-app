# Library Web App group green

### Assignments

[G5 API for the Library](https://gitlab.com/buutcampsprint/typescript2020/exercises-and-examples/-/blob/master/week-5-express/exercises/Group%20Assignment%205.md)

[G6 Library web app](https://gitlab.com/buutcampsprint/typescript2020/exercises-and-examples/-/blob/master/week-6-react/exercises/Group%20Assignments%206.md)

[G7 Library web app](https://gitlab.com/buutcampsprint/typescript2020/exercises-and-examples/-/blob/master/week-7-react/exercises/Group%20Assignments%207.md)

## Run server
```
cd server
npm install
npm run watch
```

## Start client
```
cd client
npm install
npm start
```